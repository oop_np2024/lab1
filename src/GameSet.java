public class GameSet {
    private char[][] game = new char[3][3];
    //private int row;
    //private int col;

    GameSet(){                                             // set board
        for(int i = 0; i<3; i++){
            for(int j = 0; j<3; j++){
                game[i][j] = '-';
            }
        }
    }

    void display(){
        for(int i = 0; i<3; i++){
            for(int j = 0; j<3; j++){
                System.out.print(game[i][j] + " " );
            }
        System.out.println("");
        }
    }
}
